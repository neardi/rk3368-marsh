#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/version.h>
#include <linux/fs.h>
#include <linux/proc_fs.h>
#include <asm/uaccess.h>
#include <linux/slab.h>
#include <linux/i2c.h>
#include <linux/delay.h>
#include <linux/interrupt.h>
#include <linux/input.h>
#include <linux/input/mt.h>

#include <linux/gpio.h>
#include <linux/of_gpio.h>


#include  "goodix.h"


struct goodix_dev {
    struct i2c_client       *client;
    struct input_dev        *input;

    int                     irq_pin;
    int                     pwr_pin;
    int                     rst_pin;

    int                     irq_num;
    unsigned long          irq_flags;

    struct workqueue_struct *workq;
    struct work_struct      worker;

    u16                     version;

    int                     x_max;
    int                     y_max;
};

static void goodix_reset(struct i2c_client *client)
{
    struct goodix_dev *dev = i2c_get_clientdata(client);

    pr_info("begin select I2C slave addr\n");
    gpio_direction_output(dev->rst_pin, 0);
    msleep(20);                         // T2: > 10ms
    // HIGH: 0x28/0x29, LOW: 0xBA/0xBB
    gpio_direction_output(dev->irq_pin, client->addr == 0x14);

    msleep(2);                          // T3: > 100us
    gpio_direction_output(dev->rst_pin, 1);
    msleep(6);                          // T4: > 5ms
    gpio_direction_input(dev->rst_pin);
    pr_info("begin select I2C slave addr\n");

    //int sync
    gpio_direction_output(dev->irq_pin, 0);
    msleep(50);				            /* T5: 50ms */
    gpio_direction_input(dev->irq_pin);
}

static int goodix_read(struct i2c_client *client, u8 *buf, s32 len)
{
    s32 ret = -1;
    int retry = 0;
    struct i2c_msg msgs[2];

    msgs[0].flags    = !I2C_M_RD;
    msgs[0].addr     = client->addr;
    msgs[0].len      = GTP_ADDR_LEN;
    msgs[0].buf      = &buf[0];
    msgs[0].scl_rate = 100 * 1000;
    
    msgs[1].flags    = I2C_M_RD;
    msgs[1].addr     = client->addr;
    msgs[1].len      = len - GTP_ADDR_LEN;
    msgs[1].buf      = &buf[GTP_ADDR_LEN];
    msgs[1].scl_rate = 100 * 1000;

    while(retry < 5) {
        ret = i2c_transfer(client->adapter, msgs, 2);
        if(ret == 2) break;
        retry++;
    }

    if((retry >= 5)) {
        pr_err("I2C Write: 0x%04X, %d bytes failed, errcode: %d! Process reset.", (((u16)(buf[0] << 8)) | buf[1]), len-2, ret);
        goodix_reset(client);
    }

    return ret;
}

static int goodix_write(struct i2c_client *client, u8 *buf, s32 len)
{
    int ret = -1;
    int retry = 0;
    struct i2c_msg msg;

    msg.flags    = !I2C_M_RD;
    msg.addr     = client->addr;
    msg.len      = len;
    msg.buf      = buf;
    msg.scl_rate = 200 * 1000;

    while(retry < 5) {
        ret = i2c_transfer(client->adapter, &msg, 1);
        if(ret == 1) break;
        retry++;
    }

    if((retry >= 5)) {
        pr_err("I2C Write: 0x%04X, %d bytes failed, errcode: %d! Process reset.", (((u16)(buf[0] << 8)) | buf[1]), len-2, ret);
        goodix_reset(client);
    }

    return ret;
}

static int goodix_i2c_test(struct i2c_client *client)
{
    u8 retry = 0;
    s8 ret = -1;
    u8 test[3] = {GTP_REG_CONFIG_DATA >> 8, GTP_REG_CONFIG_DATA & 0xff};

    while(retry++ < 5) {
        ret = goodix_read(client, test, 3);
        if (ret > 0) {
            return ret;
        }
        pr_err("GTP i2c test failed time %d.",retry);
        msleep(10);
    }

    return ret;
}

static int goodix_read_version(struct i2c_client *client, u16 *version)
{
    s32 ret = -1;
    u8 buf[8] = {GTP_REG_VERSION >> 8, GTP_REG_VERSION & 0xff};

    ret = goodix_read(client, buf, sizeof(buf));
    if (ret < 0) {
        pr_err("GTP read version failed");
        return ret;
    }

    if (version){
        *version = (buf[7] << 8) | buf[6];
    }

    if (buf[5] == 0x00) {
        pr_info("IC Version: %c%c%c_%02x%02x", buf[2], buf[3], buf[4], buf[7], buf[6]);
    } else {
        pr_info("IC Version: %c%c%c%c_%02x%02x", buf[2], buf[3], buf[4], buf[5], buf[7], buf[6]);
    }

    return ret;
}

#if 1
static int goodix_init_panel(struct goodix_dev *dev)
{
    s32 ret = -1;

    u8 config[GTP_CONFIG_MAX_LEN + GTP_ADDR_LEN] = {GTP_REG_CONFIG_DATA >> 8, GTP_REG_CONFIG_DATA & 0xff};

    ret = goodix_read(dev->client, config, GTP_CONFIG_MAX_LEN + GTP_ADDR_LEN);
    if (ret < 0) {
        pr_err("Read Config Failed, Using Default Resolution & INT Trigger!");
        dev->x_max = 4096;
        dev->y_max = 4096;
        dev->irq_flags = 1; // 0: Rising 1: Falling
    }

    if ((dev->x_max == 0) && (dev->y_max == 0)) {
        dev->x_max = (config[RESOLUTION_LOC + 1] << 8) + config[RESOLUTION_LOC];
        dev->y_max = (config[RESOLUTION_LOC + 3] << 8) + config[RESOLUTION_LOC + 2];
        dev->irq_flags = (config[TRIGGER_LOC]) & 0x03;
    }

    dev->irq_flags = 1;
    pr_info("X_MAX: %d, Y_MAX: %d, TRIGGER: 0x%02x", dev->x_max,dev->y_max, (u32)dev->irq_flags);
    msleep(10);
    return 0;
}
#endif

static void gtp_touch_down(struct goodix_dev *dev, s32 id,s32 x,s32 y,s32 w)
{
    input_mt_slot(dev->input, id);
    input_report_abs(dev->input, ABS_MT_TRACKING_ID, id);
    input_report_abs(dev->input, ABS_MT_POSITION_X, x);
    input_report_abs(dev->input, ABS_MT_POSITION_Y, y);
    input_report_abs(dev->input, ABS_MT_TOUCH_MAJOR, w);
    input_report_abs(dev->input, ABS_MT_WIDTH_MAJOR, w);

    pr_debug("ID:%d, X:%d, Y:%d, W:%d", id, x, y, w);
}

/*******************************************************
Function:
    Report touch release event
Input:
    ts: goodix i2c_client private data
Output:
    None.
*********************************************************/
static void gtp_touch_up(struct goodix_dev *dev, s32 id)
{
    input_mt_slot(dev->input, id);
    input_report_abs(dev->input, ABS_MT_TRACKING_ID, -1);
}


static void goodix_worker_func(struct work_struct *work)
{
    int ret = -1;
    u8 finger = 0;
    u8  touch_num = 0;

    s32 id = 0;
    s32 i  = 0;

    static u16 pre_touch = 0;
    static u8 pre_key = 0;

    u8  key_value = 0;
    u8* coor_data = NULL;

    struct goodix_dev *dev = container_of(work, struct goodix_dev, worker);

    u8  end_cmd[3] = {GTP_READ_COOR_ADDR >> 8, GTP_READ_COOR_ADDR & 0xFF, 0};
    u8  point_data[2 + 1 + 8 * GTP_MAX_TOUCH + 1] = {GTP_READ_COOR_ADDR >> 8, GTP_READ_COOR_ADDR & 0xFF};

    ret = goodix_read(dev->client, point_data, 12);
    if (ret < 0) {
        //enable irq again?
        return;
    }

    finger = point_data[GTP_ADDR_LEN];
    if (finger == 0x00) {
        //enable irq again?
        return;
    }

    if((finger & 0x80) == 0) {
        goto exit;
    }

    touch_num = finger & 0x0f;
    if (touch_num > GTP_MAX_TOUCH) {
        goto exit;
    }

    if (touch_num > 1) {
        u8 buf[8 * GTP_MAX_TOUCH] = {(GTP_READ_COOR_ADDR + 10) >> 8, (GTP_READ_COOR_ADDR + 10) & 0xff};
        ret = goodix_read(dev->client, buf, 2 + 8 * (touch_num - 1)); 
        memcpy(&point_data[12], &buf[2], 8 * (touch_num - 1));
    }

    pre_key = key_value;

    pr_debug("pre_touch:%02x, finger:%02x.", pre_touch, finger);

    if (pre_touch || touch_num) {
        s32 pos = 0;
        u16 touch_index = 0;
        u8 report_num = 0;
        coor_data = &point_data[3];
        
        if(touch_num) {
            id = coor_data[pos] & 0x0F;
            touch_index |= (0x01<<id);
        }
        
        pr_debug("id = %d,touch_index = 0x%x, pre_touch = 0x%x\n",id, touch_index,pre_touch);
        for (i = 0; i < GTP_MAX_TOUCH; i++) {
            if ((touch_index & (0x01<<i))) {
                int input_x  = coor_data[pos + 1] | (coor_data[pos + 2] << 8);
                int input_y  = coor_data[pos + 3] | (coor_data[pos + 4] << 8);
                int input_w  = coor_data[pos + 5] | (coor_data[pos + 6] << 8);

                gtp_touch_down(dev, id, input_x, input_y, input_w);
                pre_touch |= 0x01 << i;
                
                report_num++;
                if (report_num < touch_num) {
                    pos += 8;
                    id = coor_data[pos] & 0x0F;
                    touch_index |= (0x01<<id);
                }
            } else {
                gtp_touch_up(dev, i);
                pre_touch &= ~(0x01 << i);
            }
        }
    }



    input_sync(dev->input);

exit:

    ret = goodix_write(dev->client, end_cmd, 3);
    if (ret < 0) {
        pr_err("I2C write end_cmd error!");
    }

    //enable irq again
    //gtp_irq_enable(dev);
}

static irqreturn_t goodix_irq_handler(int irq, void *dev_id)
{
    struct goodix_dev *dev = dev_id;

    //pr_info("============= %s ==============\n",__func__);
    //TODO: disable irq here ?
    queue_work(dev->workq, &dev->worker);
    return IRQ_HANDLED;
}

static int goodix_probe(struct i2c_client *client, const struct i2c_device_id *id)
{
    int ret;
//    int x_max, y_max;
    struct goodix_dev *dev;
    struct input_dev *input;

    enum of_gpio_flags rst_flags, pwr_flags;

    struct device_node *np = client->dev.of_node;
    if (!np) {
        dev_err(&client->dev, "no device tree\n");
        return -EINVAL;
    }

    if (!i2c_check_functionality(client->adapter, I2C_FUNC_I2C)) {
        pr_err("check_functionality_failed\n");
        return -ENODEV;
    }

    dev = kzalloc(sizeof(*dev), GFP_KERNEL);
    input = input_allocate_device();
    if (!dev || !input) {
        pr_err("Failed to allocate driver data!\n");
        ret = -ENOMEM;
        goto err_mem;
    }

    dev->client = client;
    dev->input = input;

    input->name = client->name;
    input->id.bustype = BUS_I2C;
    input->dev.parent = &client->dev;

    input_set_drvdata(input, dev);
    i2c_set_clientdata(client, dev);

    pr_info("%s : client->addr = 0x%x. \n", __func__, client->addr);

    dev->irq_pin = of_get_named_gpio_flags(np, "touch-gpio", 0, (enum of_gpio_flags *)(&dev->irq_flags));
    dev->rst_pin = of_get_named_gpio_flags(np, "reset-gpio", 0, &rst_flags);
    dev->pwr_pin = of_get_named_gpio_flags(np, "power-gpio", 0, &pwr_flags);
#if 0
    if(of_property_read_u32(np, "max-x", &x_max)) {
        pr_err("no max-x defined \n");
        ret = -ENODEV;
        goto err_mem;
    }
    dev->x_max = x_max;
    if(of_property_read_u32(np, "max-y", &y_max)) {
        pr_err("no max-x defined \n");
        ret = -ENODEV;
        goto err_mem;
    }
    dev->y_max = y_max;
    pr_info("X_MAX: %d, Y_MAX: %d, TRIGGER: 0x%02x", dev->x_max,dev->y_max, (u32)dev->irq_flags);
#endif
    dev->irq_num = gpio_to_irq(dev->irq_pin);
    if (!dev->irq_num){
        pr_err("ts->irq  error \n");
        ret = -ENODEV;
        goto err_mem;
    }

    ret = gpio_request(dev->rst_pin, "touch-rst");
    if (ret < 0) {
        pr_err("failed to request GPIO:%d, ERRNO:%d",dev->rst_pin, ret);
        ret = -ENODEV;
        goto err_mem;
    }

    ret = gpio_request(dev->irq_pin, "touch-irq");
    if (ret < 0) {
        pr_err("failed to request GPIO:%d, ERRNO:%d",dev->irq_pin, ret);
        ret = -ENODEV;
        goto err_mem;
    }

    gpio_direction_input(dev->irq_pin);
    gpio_direction_input(dev->rst_pin);

    goodix_reset(client);

    ret = goodix_i2c_test(client);
    if (ret < 0) {
        pr_err("goodix i2c read write test error\n");
        goto err_mem;
    }

    ret = goodix_read_version(client, &dev->version);
    if (ret < 0) {
        pr_err("goodix read version error\n");
        goto err_mem;
    }

#if 1
  ret = goodix_init_panel(dev);
    if (ret < 0) {
        pr_err("goodix int panel error\n");
        goto err_mem;
    }
#endif

    __set_bit(EV_ABS, input->evbit);
    __set_bit(EV_KEY, input->evbit);
    __set_bit(EV_SYN, input->evbit);

	__set_bit(INPUT_PROP_DIRECT, input->propbit);

//    __set_bit(BTN_TOUCH, input->keybit);

    set_bit(ABS_MT_TOUCH_MAJOR, input->absbit);
    set_bit(ABS_MT_POSITION_X, input->absbit);
    set_bit(ABS_MT_POSITION_Y, input->absbit);
    set_bit(ABS_MT_WIDTH_MAJOR, input->absbit);

    input_mt_init_slots(input, GTP_MAX_TOUCH, 0);

    input_set_abs_params(input, ABS_MT_POSITION_X, 0, dev->x_max, 0, 0);
    input_set_abs_params(input, ABS_MT_POSITION_Y, 0, dev->y_max, 0, 0);
    input_set_abs_params(input, ABS_MT_TOUCH_MAJOR, 0, 255, 0, 0);
    input_set_abs_params(input, ABS_MT_WIDTH_MAJOR, 0, 200, 0, 0);

    ret = input_register_device(input);
    if (ret)
        goto err_mem;

    // owned workq and worker
    INIT_WORK(&dev->worker, goodix_worker_func);
    dev->workq = create_singlethread_workqueue("goodix_workq");
    if (!dev->workq) {
        pr_err("failed to create workqueue\n");
        ret = -EINVAL;
        goto err_mem;
    }

    // irq
	ret = request_irq(dev->irq_num, goodix_irq_handler, dev->irq_flags | IRQF_ONESHOT, DRV_NAME, dev);
	if (ret) {
		printk("Unable to request irq for device %s.\n", DRV_NAME);
		goto err_irq;
	}

    return 0;

err_irq:
    free_irq(dev->irq_num, dev);

err_mem:
    input_free_device(input);
    kfree(dev);
    return ret;
}

static int goodix_remove(struct i2c_client *client)
{
    return 0;
}




static const struct i2c_device_id goodix_id[] = {
	{ DRV_NAME, 0 },
	{}
};
MODULE_DEVICE_TABLE(i2c, goodix_id);

static struct of_device_id goodix_dt_ids[] = {
    { .compatible = "goodix,gt911"},
    { .compatible = "goodix,gt9271"},
    { }
};


static struct i2c_driver goodix_driver = {
    .probe		= goodix_probe,
    .remove		= goodix_remove,
    .id_table	= goodix_id,
    .driver	= {
        .name	= DRV_NAME,
        .owner	= THIS_MODULE,
        .of_match_table = of_match_ptr(goodix_dt_ids),
    },
};

static int goodix_init(void)
{
	return i2c_add_driver(&goodix_driver);
}


static void goodix_exit(void)
{
	i2c_del_driver(&goodix_driver);
}

module_init(goodix_init);
module_exit(goodix_exit);
MODULE_LICENSE("GPL");
