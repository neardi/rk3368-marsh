#ifndef _GOODIX_GT9XX_H_
#define _GOODIX_GT9XX_H_

#define DRV_NAME            "goodix-tp"

#define GTP_MAX_TOUCH       10


#define GTP_ADDR_LEN        2       // address length
#define GTP_CONFIG_MIN_LEN  186
#define GTP_CONFIG_MAX_LEN  240

// Registers define
#define GTP_READ_COOR_ADDR    0x814E
#define GTP_REG_SLEEP         0x8040
#define GTP_REG_SENSOR_ID     0x814A
#define GTP_REG_CONFIG_DATA   0x8047
#define GTP_REG_VERSION       0x8140

#define RESOLUTION_LOC        3
#define TRIGGER_LOC           8



#endif
